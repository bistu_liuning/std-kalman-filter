#ifndef	__ALGORITHM_H__
#define	__ALGORITHM_H__

void Init_Attitude(void);
void Attitude_Solve(double Wx,double Wy,double Wz);
void SINS_Solve(double Ax,double Ay,double Az);
void Reload_Nav_Pra(void);
void Nav_Kalman(void);
void Init_Alignment(float fxb,float fyb,float fzb);

#endif