#include "define.h"
/***********************************************************
函数名称：void Init_Attitude(double Wx,double Wy,double Wz
函数功能：初始姿态矩阵和相关四元数值
入口参数：
出口参数：
函数说明：俯仰：[-90,90]theta、航向角：[-180,180]phi、滚转角：[-180,180]gamma
************************************************************/
void Init_Attitude(void)
{			

	Cbn[0][0]=cos(Temp_theta)*cos(Temp_phi);
	Cbn[0][1]=sin(Temp_gamma)*sin(Temp_theta)*cos(Temp_phi)-cos(Temp_gamma)*sin(Temp_phi);
	Cbn[0][2]=cos(Temp_gamma)*sin(Temp_theta)*cos(Temp_phi)+sin(Temp_gamma)*sin(Temp_phi);
	Cbn[1][0]=cos(Temp_theta)*sin(Temp_phi);
	Cbn[1][1]=sin(Temp_gamma)*sin(Temp_theta)*sin(Temp_phi)+cos(Temp_gamma)*cos(Temp_phi);
	Cbn[1][2]=cos(Temp_gamma)*sin(Temp_theta)*sin(Temp_phi)-sin(Temp_gamma)*cos(Temp_phi);
	Cbn[2][0]=-sin(Temp_theta);
	Cbn[2][1]=sin(Temp_gamma)*cos(Temp_theta);
	Cbn[2][2]=cos(Temp_gamma)*cos(Temp_theta);   //%B->n   载体坐标系到导航坐标系

	q[0]=0.5*sqrt(1+Cbn[0][0]+Cbn[1][1]+Cbn[2][2]);
	q[1]=0.25*(Cbn[2][1]-Cbn[1][2])/q[0];
	q[2]=0.25*(Cbn[0][2]-Cbn[2][0])/q[0];
	q[3]=0.25*(Cbn[1][0]-Cbn[0][1])/q[0];
	A=sqrt(q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3]);
	q[0]=q[0]/A;
	q[1]=q[1]/A;
	q[2]=q[2]/A;
	q[3]=q[3]/A;
}
/************************************************************
函数名称：void Attitude_Solve(double Wx,double Wy,double Wz
函数功能：求解姿态角
入口参数：Wx 度/秒 x轴角速度
		  Wy 度/秒 y轴角速度
		  Wz 度/秒 z轴角速度
出口参数：输出三个姿态角，姿态角范围：俯仰：[-90,90]、航向角：[-180,180]、滚转角：[-180,180]。
函数说明：
*************************************************************/
void Attitude_Solve(double Wx,double Wy,double Wz)
{
    INT8U i;
	Wx=Wx*PI/180;
	Wy=Wy*PI/180;
	Wz=Wz*PI/180;

	DATA=sqrt((Wx*Tm)*(Wx*Tm)+(Wy*Tm)*(Wy*Tm)+(Wz*Tm)*(Wz*Tm)); 	  //计算角增量
	//更新四元数
	q1[0]=((1-DATA*DATA/8+DATA*DATA*DATA*DATA/384)*q[0]-Wx*Tm*(0.5-DATA*DATA/48)*q[1]-Wy*Tm*(0.5-DATA*DATA/48)*q[2]-Wz*Tm*(0.5-DATA*DATA/48)*q[3]);//A;
	q1[1]=((1-DATA*DATA/8+DATA*DATA*DATA*DATA/384)*q[1]+Wx*Tm*(0.5-DATA*DATA/48)*q[0]+Wz*Tm*(0.5-DATA*DATA/48)*q[2]-Wy*Tm*(0.5-DATA*DATA/48)*q[3]);//A;
	q1[2]=((1-DATA*DATA/8+DATA*DATA*DATA*DATA/384)*q[2]+Wy*Tm*(0.5-DATA*DATA/48)*q[0]-Wz*Tm*(0.5-DATA*DATA/48)*q[1]+Wx*Tm*(0.5-DATA*DATA/48)*q[3]);//A;
	q1[3]=((1-DATA*DATA/8+DATA*DATA*DATA*DATA/384)*q[3]-Wx*Tm*(0.5-DATA*DATA/48)*q[2]+Wy*Tm*(0.5-DATA*DATA/48)*q[1]+Wz*Tm*(0.5-DATA*DATA/48)*q[0]);//A;
	for(i=0;i<4;i++)
	{
	   	 q[i]=q1[i];
	}
	//四元数归一操作
    A=sqrt(q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3]);
	q[0]=q[0]/A;
	q[1]=q[1]/A;
	q[2]=q[2]/A;
	q[3]=q[3]/A;
	//计算方向余弦矩阵
	Cbn[0][0]=q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3];   
	Cbn[0][1]=2*(q[1]*q[2]-q[0]*q[3]);
	Cbn[0][2]=2*(q[1]*q[3]+q[0]*q[2]);
	Cbn[1][0]=2*(q[1]*q[2]+q[0]*q[3]);
	Cbn[1][1]=q[0]*q[0]-q[1]*q[1]+q[2]*q[2]-q[3]*q[3];
	Cbn[1][2]=2*(q[2]*q[3]-q[0]*q[1]);
	Cbn[2][0]=2*(q[1]*q[3]-q[0]*q[2]);                    
	Cbn[2][1]=2*(q[2]*q[3]+q[0]*q[1]); 
	Cbn[2][2]=q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3];	   
    
    Temp_theta=-asin(Cbn[2][0]);             //%俯仰角范围为：[-90,90],无需探讨主值区间 
    if((Temp_theta<PI/2)&&((Temp_theta>-PI/2)))		 //排除T俯仰的两个极点
    {   
	   if(Cbn[0][0]!=0)
       {
			Temp_phi=atan(Cbn[1][0]/Cbn[0][0]);      //%航向角范围为：[-180,180]
			if (fabs(Cbn[0][0])<espp)
			{                
			    if(Cbn[1][0]>0)
				{
			        Temp_phi=PI/2;
				}
			    else if(Cbn[1][0]<0)
				{
			        Temp_phi=-PI/2;
			    }
			}
			else if(Cbn[0][0]>espp)
			{
		        Temp_phi=Temp_phi;
			}
			else if(Cbn[0][0]<-espp)
			{
			    if(Cbn[1][0]>0)
			    {
				    Temp_phi=Temp_phi+PI;
			    }
				else if(Cbn[1][0]<0)
			    {
				    Temp_phi=Temp_phi-PI;
			    }
				else if(Cbn[1][0]==0)
			    {
				    Temp_phi=Temp_phi;
			    }
			}
		}
        if(Cbn[2][2]!=0)
        {   
		    Temp_gamma=atan(Cbn[2][1]/Cbn[2][2]);      //%滚转角范围为：[-180,180]
            if(fabs(Cbn[2][2])<espp)
			{                
                if(Cbn[2][1]>0)
				{
                    Temp_gamma=PI/2;
                }
				else if(Cbn[2][1]<0)
                {
				    Temp_gamma=-PI/2;
                }
            }
			else if(Cbn[2][2]>espp)
			{
                Temp_gamma=Temp_gamma;
            }
			else if(Cbn[2][2]<-espp)
			{
                if(Cbn[2][1]>0)
				{
                    Temp_gamma=Temp_gamma+PI;
                }
				else if(Cbn[2][1]<0)
				{
                    Temp_gamma=Temp_gamma-PI;
                }
				else if(Cbn[2][1]==0)
                {
				    Temp_gamma=Temp_gamma;
                }
            }
        }
	}
    else
	{
        Temp_phi=(atan((Cbn[1][2]-Cbn[0][1])/(Cbn[0][2]+Cbn[1][1]))+atan((Cbn[1][2]+Cbn[0][1])/(Cbn[0][2]-Cbn[1][1])))/2;
        Temp_gamma=(atan((Cbn[1][2]-Cbn[0][1])/(Cbn[0][2]+Cbn[1][1]))-atan((Cbn[1][2]+Cbn[0][1])/(Cbn[0][2]-Cbn[1][1])))/2;
    }
}

/************************************************************
函数名称：void SINS_Solve(double Wx,double Wy,double Wz
函数功能：求解速度
入口参数：Ax 米/秒方 x轴加速度
		  Ay 米/秒方 y轴加速度
		  Az 米/秒方 z轴加速度
出口参数：输出三个轴向速度
函数说明：
*************************************************************/
void SINS_Solve(double Ax,double Ay,double Az)
{
	//计算导航坐标系内加速度
	fn=Cbn[0][0]*Ax+Cbn[0][1]*Ay+Cbn[0][2]*Az+0;
	fe=Cbn[1][0]*Ax+Cbn[1][1]*Ay+Cbn[1][2]*Az+0;
	fd=Cbn[2][0]*Ax+Cbn[2][1]*Ay+Cbn[2][2]*Az-g0;
 
    //Temp_Vn=Temp_Vn+Ax1*Tm*cos(Temp_phi);
	//Temp_Ve=Temp_Ve+Ax1*Tm*sin(Temp_phi);
	//计算北东地速度，此处可加相关补偿算法，如补偿地球自转角速率等
	Temp_Vn=Temp_Vn+fn*Tm;
    Temp_Ve=Temp_Ve+fe*Tm;
	Temp_Vd=Temp_Vd+fd*Tm;

	Temp_Sn=Temp_Sn+Temp_Vn*Tm;
	Temp_Se=Temp_Se+Temp_Ve*Tm;
	Temp_Sd=Temp_Sd+Temp_Vd*Tm;
	

	//计算经纬高
	Temp_La=Temp_La+Temp_Vn*Tm/(R+Temp_Al);		          //计算纬度
	Temp_Lo=Temp_Lo-Temp_Ve*Tm/(R+Temp_Al)/cos(Temp_La);  //计算经度
	Temp_Al=Temp_Al+Temp_Vd*Tm;							  //计算高度
 
}

/****************************************************************
函数名称:void Reload_Nav_Pra(void)
函数功能:重载导航参数
入口参数:无
出口参数:无
备注：
*****************************************************************/
void Reload_Nav_Pra(void)
{
	double L=Temp_La;                  //导入INS计算纬度
	double Rn,Rm;
	double h=Temp_Al;                  //导入INS计算高度
	int i,j;

	double fu=fd;
	double Vu=Vd;

	Rm = R*(1-2*e+3*e*sin(L)*sin(L));   //计算子午圈半径
	Rn = R*(1-e*sin(L)*sin(L));         //卯酉圈半径

	Nav_A[0][1]      = wie*sin(L)+Ve*tan(L)/(Rn+h);
	Nav_A[0][2]      = -(wie*cos(L)+Ve/(Rn+h));
	Nav_A[0][4]      = -1/(Rm+h);
	Nav_A[0][8]      = Vn/((Rm+h)*(Rm+h));
	Nav_A[1][0]      = -(wie*sin(L)+Ve*tan(L)/(Rn+h));
	Nav_A[1][2]      = -Vn/(Rm+h);
	Nav_A[1][3]      = 1/(Rn+h);
	Nav_A[1][6]      = -wie*sin(L);
	Nav_A[1][8]      = -Ve/((Rn+h)*(Rn+h));
	Nav_A[2][0]      = wie*cos(L)+Ve/(Rn+h);
	Nav_A[2][1]      = Vn/(Rm+h);
	Nav_A[2][3]      = tan(L)/(Rn+h);
	Nav_A[2][6]      = wie*cos(L)+Ve*(1/(cos(L)*cos(L)))/(Rn+h);
	Nav_A[2][8]      = -Ve*tan(L)/((Rn+h)*(Rn+h));
	Nav_A[3][1]      = -fu;
	Nav_A[3][2]      = fn;
	Nav_A[3][3]      = Vn*tan(L)/(Rm+h)-Vu/(Rm+h);
	Nav_A[3][4]      = 2*wie*sin(L)+Ve*tan(L)/(Rn+h);
	Nav_A[3][5]      = -(2*wie*cos(L)+Ve/(Rn+h));
	Nav_A[3][6]      = 2*wie*cos(L)*Vn+Ve*Vn*(1/cos(L)*1/cos(L))/(Rn+h)+2*wie*sin(L)*Vu;
	Nav_A[3][8]      = (Ve*Vu-Ve*Vn*tan(L))/((Rn+h)*(Rn+h));
	Nav_A[4][0]      = fu;
	Nav_A[4][2]      = -fe;
	Nav_A[4][3]      = -2*(wie*sin(L)+Ve*tan(L)/(Rn+h));
	Nav_A[4][4]      = -Vu/(Rm+h);
	Nav_A[4][5]      = -Vn/(Rm+h);
	Nav_A[4][6]      = -(2*wie*cos(L)+Ve*(1/(cos(L)*cos(L))/(Rn+h)))*Ve;
	Nav_A[4][8]      = (Ve*Ve*tan(L)+Vn*Vu)/((Rn+h)*(Rn+h));
	Nav_A[5][0]      = -fn;
	Nav_A[5][1]      = fe;
	Nav_A[5][3]      = 2*(wie*cos(L)+Ve/(Rn+h));
	Nav_A[5][4]      = 2*Vn/(Rm+h);
	Nav_A[5][6]      = -2*Ve*wie*sin(L);
	Nav_A[5][8]      = -(Vn*Vn+Ve*Ve)/((Rn+h)*(Rn+h));
	Nav_A[6][4]      = 1/(Rm+h);
	Nav_A[6][8]      = -Vn/((Rm+h)*(Rm+h));
	Nav_A[7][3]      = 1/((Rn+h)*cos(L));
	Nav_A[7][6]      = Ve*tan(L)/((Rn+h)*cos(L));
	Nav_A[7][8]      = -Ve/(cos(L)*(Rn+h)*(Rn+h));
	Nav_A[8][5]      = 1;

	Nav_A[0][9]      = Cnb[0][0];
	Nav_A[0][10]     = Cnb[0][1];
	Nav_A[0][11]     = Cnb[0][2];
	Nav_A[1][9]      = Cnb[1][0];
	Nav_A[1][10]     = Cnb[1][1];
	Nav_A[1][11]     = Cnb[1][2];
	Nav_A[2][9]      = Cnb[2][0];
	Nav_A[2][10]     = Cnb[2][1];
	Nav_A[2][11]     = Cnb[2][2];

	Nav_A[0][12]     = Cnb[0][0];
	Nav_A[0][13]     = Cnb[0][1];
	Nav_A[0][14]     = Cnb[0][2];
	Nav_A[1][12]     = Cnb[1][0];
	Nav_A[1][13]     = Cnb[1][1];
	Nav_A[1][14]     = Cnb[1][2];
	Nav_A[2][12]     = Cnb[2][0];
	Nav_A[2][13]     = Cnb[2][1];
	Nav_A[2][14]     = Cnb[2][2];

	Nav_A[3][15]     = Cnb[0][0];
	Nav_A[3][16]     = Cnb[0][1];
	Nav_A[3][17]     = Cnb[0][2];
	Nav_A[4][15]     = Cnb[1][0];
	Nav_A[4][16]     = Cnb[1][1];
	Nav_A[4][17]     = Cnb[1][2];
	Nav_A[5][15]     = Cnb[2][0];
	Nav_A[5][16]     = Cnb[2][1];
	Nav_A[5][17]     = Cnb[2][2];

	Nav_A[12][12]    = -(double)1/Tg[0];
	Nav_A[13][13]    = -(double)1/Tg[1];
	Nav_A[14][14]    = -(double)1/Tg[2];
	Nav_A[15][15]    = -(double)1/Ta[0];
	Nav_A[16][16]    = -(double)1/Ta[1];
	Nav_A[17][17]    = -(double)1/Ta[2];


	B[0][0]=Cnb[0][0];
	B[0][1]=Cnb[0][1];
	B[0][2]=Cnb[0][2];
	B[1][0]=Cnb[1][0];
	B[1][1]=Cnb[1][1];
	B[1][2]=Cnb[1][2];
	B[2][0]=Cnb[2][0];
	B[2][1]=Cnb[2][1];
	B[2][2]=Cnb[2][2];

	B[12][3]=Unit_Array_3[0][0];   //赋值3维单位矩阵
	B[12][4]=Unit_Array_3[0][1];
	B[12][5]=Unit_Array_3[0][2];
	B[13][3]=Unit_Array_3[1][0];
	B[13][4]=Unit_Array_3[1][1];
	B[13][5]=Unit_Array_3[1][2];
	B[14][3]=Unit_Array_3[2][0];
	B[14][4]=Unit_Array_3[2][1];
	B[14][5]=Unit_Array_3[2][2];

	B[15][6]=Unit_Array_3[0][0];   //赋值3维单位矩阵
	B[15][7]=Unit_Array_3[0][1];
	B[15][8]=Unit_Array_3[0][2];
	B[16][6]=Unit_Array_3[1][0];
	B[16][7]=Unit_Array_3[1][1];
	B[16][8]=Unit_Array_3[1][2];
	B[17][6]=Unit_Array_3[2][0];
	B[17][7]=Unit_Array_3[2][1];
	B[17][8]=Unit_Array_3[2][2];

	Nav_H[0][6]=1;
	Nav_H[1][7]=1;
	Nav_H[2][8]=1;
}

/****************************************************************
函数名称:void Nav_Kalman(void)
函数功能:进入kalman滤波
入口参数:无
出口参数:无
备注：
*****************************************************************/
void Nav_Kalman(void)
{
// 	double B[KALMAN_DEM][KALMAN_NOISE]={0};
// 	double H[KALMAN_OBSERVE][KALMAN_DEM]={0};
// 	double F[KALMAN_DEM][KALMAN_DEM]={0};
	double Temp_1818_1[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_2[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_3[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_4[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_5[KALMAN_DEM][KALMAN_DEM]={0};
	double Temp_1818_6[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_7[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_8[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_9[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_10[KALMAN_DEM][KALMAN_DEM]={0};
	double Temp_1818_11[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_12[KALMAN_DEM][KALMAN_DEM]={0};
	double Temp_189_1[KALMAN_DEM][KALMAN_NOISE]={0},Temp_189_2[KALMAN_DEM][KALMAN_NOISE]={0};
	double Temp_183_1[KALMAN_DEM][KALMAN_OBSERVE]={0},Temp_183_2[KALMAN_DEM][KALMAN_OBSERVE]={0};
	double Temp_918_1[KALMAN_NOISE][KALMAN_DEM]={0};
	double Temp_33_1[KALMAN_OBSERVE][KALMAN_OBSERVE]={0},Temp_33_2[KALMAN_OBSERVE][KALMAN_OBSERVE]={0},Temp_33_3[KALMAN_OBSERVE][KALMAN_OBSERVE]={0};
	double Temp_181_1[KALMAN_DEM]={0},Temp_181_2[KALMAN_DEM]={0};
	double Temp_31_1[KALMAN_OBSERVE]={0},Temp_31_2[KALMAN_OBSERVE]={0};
// 	double *Temp_1818_1,*Temp_1818_2,*Temp_1818_3;
// 	double *Temp_189_1;
// 	double *Temp_183_1,*Temp_183_2;
// 	double *Temp_918_1;
// 	double *Temp_33_1,*Temp_33_2;
// 	double *Temp_181_1;
// 	double *Temp_31_1;

// 	Temp_1818_1=(double *)malloc(KALMAN_DEM*KALMAN_DEM*sizeof(double));
// 	Temp_1818_2=(double *)malloc(KALMAN_DEM*KALMAN_DEM*sizeof(double));
// 	Temp_1818_3=(double *)malloc(KALMAN_DEM*KALMAN_DEM*sizeof(double));
// 	Temp_189_1=(double *)malloc(KALMAN_DEM*KALMAN_NOISE*sizeof(double));
// 	Temp_183_1=(double *)malloc(KALMAN_DEM*KALMAN_OBSERVE*sizeof(double));
// 	Temp_183_2=(double *)malloc(KALMAN_DEM*KALMAN_OBSERVE*sizeof(double));
// 	Temp_918_1=(double *)malloc(KALMAN_NOISE*KALMAN_DEM*sizeof(double));
// 	Temp_33_1=(double *)malloc(KALMAN_OBSERVE*KALMAN_OBSERVE*sizeof(double));
// 	Temp_33_2=(double *)malloc(KALMAN_OBSERVE*KALMAN_OBSERVE*sizeof(double));
// 	Temp_181_1=(double *)malloc(KALMAN_DEM*sizeof(double));
// 	Temp_31_1=(double *)malloc(KALMAN_OBSERVE*sizeof(double));
	/*
	%每秒更新一次速度位置误差
	%连续状态系统方程
	%dx = A*x + B*w
	%z = C*x + v
	%离散状态系统方程
	%x(k+1) = F*x(k) + G*w(k)
	%z(k+1) = H*x(k+1) + v(k+1)
	*/
	//进入KALMAN滤波程序
	/*
	连续系统离散化
	F= eye(18,18)+A*tao;
	*/
	Matrix_Mul_Digit((double *)Nav_A,  //A的维数：18行 18列
		              KALMAN_DEM,      //行
					  KALMAN_DEM,      //列
					  NAV_Tm,		   //矩阵A乘以积分时间
					  (double *)Temp_1818_1
					  );  //A*tao
	Matrix_Add((double *)Unit_Array_18,  //单位矩阵eye(18,18)
		       (double *)Temp_1818_1,    //加上A*Tao
		       KALMAN_DEM,
		       KALMAN_DEM,
		       (double *)Nav_F
			   );//eye(18,18)+A*tao

    //G=(eye(18,18)+tao*A/2)*B*tao;
	Matrix_Mul_Digit((double *)Nav_A,
					KALMAN_DEM,
					KALMAN_DEM,
					NAV_Tm/2,
					(double *)Temp_1818_2
					);  //A*tao/2
	Matrix_Add((double *)Unit_Array_18,
				(double *)Temp_1818_2,     //A*tao/2
				KALMAN_DEM,
				KALMAN_DEM,
				(double *)Temp_1818_3   //18*18
				);//eye(18,18)+A*tao/2
	Matrix_Mul((double *)Temp_1818_3,
		       (double *)B,
			   KALMAN_DEM,     //输出为18行，9列
			   KALMAN_DEM,
			   KALMAN_NOISE,
			   (double *)Temp_189_1     //18*9
			   );//(eye(18,18)+tao*A/2)*B
	Matrix_Mul_Digit((double *)Temp_189_1,
		             KALMAN_DEM,
					 KALMAN_NOISE,
					 (double)NAV_Tm,
					 (double *)Nav_G   //18*9
					);//(eye(18,18)+tao*A/2)*B*tao
// 	//离散化结束
// 	//计算协方差P
// 	//P = F*(PP0)*F'+G*Q*G';
	Matrix_Mul((double *)Nav_F,       //计算F*PP0
		       (double *)Nav_P0,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   (double *)Temp_1818_4
		       );//F*(PP0)
	Matrix_Inver((double *)Nav_F,
		         KALMAN_DEM,
				 KALMAN_DEM,
				 (double *)Temp_1818_5
				 );//F'
	Matrix_Mul((double *)Temp_1818_4,
		       (double *)Temp_1818_5,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   (double *)Temp_1818_6
			   ); //F*(PP0)*F'
    Matrix_Mul((double *)Nav_G,      //G的维数：18行 9列
		        (double *)Nav_Q,     //G的维数：9行 9列 
				KALMAN_DEM,
				KALMAN_NOISE,
				KALMAN_NOISE,
				(double *)Temp_189_2          //18*9
		       );//G*Q
	Matrix_Inver((double *)Nav_G,
				KALMAN_DEM,
				KALMAN_NOISE,
				(double *)Temp_918_1                 //9*18
				);//G'
	Matrix_Mul((double *)Temp_189_2,
			   (double *)Temp_918_1,
			   KALMAN_DEM,
			   KALMAN_NOISE,
			   KALMAN_DEM,
			   (double *)Temp_1818_7  //18*18
			  );//G*Q*G'

	Matrix_Add((double *)Temp_1818_6,
		       (double *)Temp_1818_7,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   (double *)Nav_P
			   );//P = F*(PP0)*F'+G*Q*G';

	//计算增益
	//K = P*H'*inv(H*P*H'+R);
	Matrix_Inver((double *)Nav_H,
		          KALMAN_OBSERVE,   //KALMAN滤波观测器输出维数
				  KALMAN_DEM,
				  (double *)Temp_183_1  //KALMAN_DEM*KALMAN_OBSERVE 18*3
				);//H'
	Matrix_Mul((double *)Nav_P,
		       (double *)Temp_183_1,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   KALMAN_OBSERVE,
			   (double *)Temp_183_2    //18*3
			   );//P*H'
    Matrix_Mul((double *)Nav_H,
		       (double *)Temp_183_2,
			   KALMAN_OBSERVE,
			   KALMAN_DEM,
			   KALMAN_OBSERVE,
			   (double *)Temp_33_1 //3*3
			   );//H*P*H'
	Matrix_Add((double *)Temp_33_1,
		       (double *)Nav_R,
			   KALMAN_OBSERVE,
			   KALMAN_OBSERVE,
			   (double *)Temp_33_2
			   );//(H*P*H'+R)
	Matrix_Opp((double *)Temp_33_2,
		        KALMAN_OBSERVE,
				(double *)Temp_33_3
				);//inv(H*P*H'+R);
	Matrix_Mul((double *)Temp_183_2,
		       (double *)Temp_33_3,
			   KALMAN_DEM,
			   KALMAN_OBSERVE,
			   KALMAN_OBSERVE,
			   (double *)Nav_K
			   );//P*H'*inv(H*P*H'+R);
	//PP0 = (eye(18,18)-K*H)*P;
	Matrix_Mul((double *)Nav_K,
		       (double *)Nav_H,
			   KALMAN_DEM,
			   KALMAN_OBSERVE,
			   KALMAN_DEM,
			   (double *)Temp_1818_8
		      ); //K*H
	Matrix_Sub((double *)Unit_Array_18,
		        (double *)Temp_1818_8,
		        KALMAN_DEM,
				KALMAN_DEM,
				(double *)Temp_1818_9
		       );//eye(18,18)-K*H
	Matrix_Mul((double *)Temp_1818_9,
		       (double *)Nav_P,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   (double *)Temp_1818_12
			   );//(eye(18,18)-K*H)*P
	//PP0 = (PP0+PP0')/2;
	Matrix_Inver((double *)Temp_1818_12,
		          KALMAN_DEM,
				  KALMAN_DEM,
				  (double *)Temp_1818_10
				  );
	Matrix_Add((double *)Temp_1818_12,
		        (double *)Temp_1818_10,
		        KALMAN_DEM,
			    KALMAN_DEM,
				(double *)Temp_1818_11
			   );
	Matrix_Mul_Digit((double *)Temp_1818_11,
		             KALMAN_DEM,
					 KALMAN_DEM,
					 (double)0.5,
					 (double *)Nav_P0
					 );
//	XX = A*X+K*(z-H*A*X);
//	X = XX;
	Matrix_Mul((double *)Nav_F,
		       (double *)Nav_X,
			   KALMAN_DEM,
			   KALMAN_DEM,
			   1,
			   (double *)Temp_181_1
		       );//A*X
	Matrix_Mul((double *)Nav_H,
		       (double *)Temp_181_1,
			   KALMAN_OBSERVE,
			   KALMAN_DEM,
			   1,
			   (double *)Temp_31_1
			   );//H*A*X
	Matrix_Sub((double *)Nav_Z,
		        (double *)Temp_31_1,
				KALMAN_OBSERVE,
				1,
				(double *)Temp_31_2
				);//z-H*A*X
	Matrix_Mul((double *)Nav_K,
	            (double *)Temp_31_2,
				KALMAN_DEM,
				KALMAN_OBSERVE,
				1,
				(double *)Temp_181_2
	           );//K*(z-H*A*X)
	Matrix_Add((double *)Temp_181_1,
		       (double *)Temp_181_2,
			   KALMAN_DEM,
			   1,
			   (double *)Nav_X
			   );

	E_Attitude[0]=Nav_X[0];   //获得姿态误差估计值
	E_Attitude[1]=Nav_X[1];
	E_Attitude[2]=Nav_X[2];
	E_Velocity[0]=Nav_X[3];   //获得速度误差估计值
	E_Velocity[1]=Nav_X[4];
	E_Velocity[2]=Nav_X[5];
	E_Position[0]=Nav_X[6];   //获得位置误差估计值
	E_Position[1]=Nav_X[7];
	E_Position[2]=Nav_X[8];
	printf("%5.10f,%5.10f,%5.10f\n",Nav_X[6],Nav_X[7],Nav_X[8]);
}
/****************************************************************
函数名称:void Init_Alignment(void)
函数功能:初始对准
入口参数:无
出口参数:无
备注：
*****************************************************************/
void Init_Alignment(float fxb,float fyb,float fzb)
{
    FP32 g=0,Hx=0,Hy=0,Hz=0;
    g=sqrt(fxb*fxb+fyb*fyb+fzb*fzb);   //计算当地加速度
    Temp_phi=0;                        //偏航角输出为0
    Temp_theta=asin(fxb/g);            //计算俯仰角
    if(((Temp_theta*180/PI)>89) || ((Temp_theta*180/PI)<-89))   //俯仰角在正负89度之间，横滚角均为0度
    {
       Temp_gamma=0; 
    }
    else
    {
        if(fzb!=0)
        {   
		    Temp_gamma=atan(fyb/fzb);      //%滚转角范围为：[-180,180]
            if(fabs(fzb)<espp)
			{                
                if(fyb>0)
				{
                    Temp_gamma=PI/2;
                }
				else if(fyb<0)
                {
				    Temp_gamma=-PI/2;
                }
            }
			else if(fzb>espp)
			{
                Temp_gamma=Temp_gamma;
            }
			else if(fzb<-espp)
			{
                if(fyb>0)
				{
                    Temp_gamma=Temp_gamma+PI;
                }
				else if(fyb<0)
				{
                    Temp_gamma=Temp_gamma-PI;
                }
				else if(fyb==0)
                {
				    Temp_gamma=Temp_gamma;
                }
            }
        }
    }
	Temp_gamma=-Temp_gamma;
    Cbn[0][0]=cos(Temp_theta)*cos(Temp_phi);
    Cbn[0][1]=sin(Temp_gamma)*sin(Temp_theta)*cos(Temp_phi)-cos(Temp_gamma)*sin(Temp_phi);
    Cbn[0][2]=cos(Temp_gamma)*sin(Temp_theta)*cos(Temp_phi)+sin(Temp_gamma)*sin(Temp_phi);
    Cbn[1][0]=cos(Temp_theta)*sin(Temp_phi);
    Cbn[1][1]=sin(Temp_gamma)*sin(Temp_theta)*sin(Temp_phi)+cos(Temp_gamma)*cos(Temp_phi);
    Cbn[1][2]=cos(Temp_gamma)*sin(Temp_theta)*sin(Temp_phi)-sin(Temp_gamma)*cos(Temp_phi);
    Cbn[2][0]=-sin(Temp_theta);
    Cbn[2][1]=sin(Temp_gamma)*cos(Temp_theta);
    Cbn[2][2]=cos(Temp_gamma)*cos(Temp_theta);   //%B->n   载体坐标系到导航坐标系
	//将此信号倒换到大地水平坐标系内
	Hx=Cbn[0][0]*MAG_X+Cbn[0][1]*MAG_Y+Cbn[0][2]*MAG_Z;
	Hy=Cbn[1][0]*MAG_X+Cbn[1][1]*MAG_Y+Cbn[1][2]*MAG_Z;
	Hz=Cbn[2][0]*MAG_X+Cbn[2][1]*MAG_Y+Cbn[2][2]*MAG_Z;

    //计算偏航角
	if(Hx!=0)
	{
		Temp_phi=atan(-Hy/Hx);	//%航向角范围为：[-180,180]    
        if(fabs(Hx)<espp)
		{                
            if(-Hy>0)
			{
                Temp_phi=PI/2;
            }
			else if(-Hy<0)
            {
			    Temp_phi=-PI/2;
            }
        }
		else if(Hx>espp)
		{
            Temp_phi=Temp_phi;
        }
		else if(Hx<-espp)
		{
            if(-Hy>0)
			{
                Temp_phi=Temp_phi+PI;
            }
			else if(-Hy<0)
			{
                Temp_phi=Temp_phi-PI;
            }
			else if(-Hy==0)
            {
			    Temp_phi=Temp_phi;
            }
        }
	}
	else
	{
	   	Temp_phi=PI;
	}
	Temp_phi=Temp_phi;
	//Kalman滤波
//    P_gamma=A_gamma*P_gamma*A_gamma+B_gamma*Q_gamma*B_gamma;
//    K_gamma=P_gamma*C_gamma*1/(C_gamma*P_gamma*C_gamma+R_gamma);
 //   P_gamma=(1-K_gamma*C_gamma)*P_gamma;
 //   X_gamma=A_gamma*X_gamma+K_gamma*(Temp_gamma-C_gamma*A_gamma*X_gamma);
 //   Temp_gamma=C_gamma*X_gamma;
}