/************************************************************
  Copyright (C), 2009-2016, 北京信息科技大学智能控制研究所.
  FileName: Navigation.cpp
  Author:   刘宁    
  Version : 1.0          
  Date:     2012.4.13
  Description:     实现组合导航系统的C语言仿真环境      
  Function List:   // 主要函数及其功能
    1. void Matrix_Add(...) 矩阵相加
	2.
	3.
  History:         // 历史修改记录
      <author>  <time>   <version >   <desc>
      David    96/10/12     1.0     build this moudle  
***********************************************************/
#include "define.h"

float time=0;         //系统时标
float Gx=0;           //x轴陀螺仪输出
float Gy=0;           //y轴陀螺仪输出
float Gz=0;           //y轴陀螺仪输出

float Ax=0;           //x轴加速度计输出
float Ay=0;           //y轴加速度计输出
float Az=0;           //y轴加速度计输出

float fn=0;
float fe=0;
float fd=0;

float MAG_X=0;
float MAG_Y=0;
float MAG_Z=0;

float Phi=0;          //偏航角输出
float Theta=0;        //俯仰角输出
float Gamma=0;        //横滚角输出

float Vn=0;           //北向速度
float Ve=0;           //东向速度
float Vd=0;           //地向速度

float Output_La=0;   //组合后纬度输出
float Output_Lo=0;   //组合后经度输出
float Output_Al=0;   //组合后高度输出

float Temperature=0;  //系统温度输出
float Satellite_Num=0;//卫星数
float Gps_La=0;       //GPS纬度输出
float Gps_Lo=0;       //GPS经度输出
float Gps_UTC=0;      //GPS UTC时间


//姿态解算
FP64 A=0;	            //四元数归一参考值
FP64 q1[4]={0};
FP32 q[4]={0}; //存储四元数
FP64 T[3][3]={0};      //转移矩阵
FP64 Cbn[3][3]={0},Cnb[3][3]={0};    //方向余弦矩阵
FP64 Temp_phi=0,Temp_theta=0,Temp_gamma=0;		//三个姿态角
FP64 Temp_Vn=0,Temp_Ve=0,Temp_Vd=0;             //三个轴向速度
FP64 Temp_Sn=0,Temp_Se=0,Temp_Sd=0;             //三个轴向位置
FP64 Temp_La=0,Temp_Lo=0,Temp_Al=0;             //经纬高
FP64 DATA=0;
FP64 Tm=0.02;          //姿态解算更新时间为20ms

FP64 Unit_Array_18[KALMAN_DEM][KALMAN_DEM]={0},Unit_Array_3[KALMAN_OBSERVE][KALMAN_OBSERVE]={0};
FP64 NAV_Tm=1;    //kalman滤波器更新时间
FP64 Nav_X[KALMAN_DEM]={0},Nav_Z[KALMAN_OBSERVE]={0};    //输入状态向量 7维
FP64 Nav_A[KALMAN_DEM][KALMAN_DEM]={0},Nav_F[KALMAN_DEM][KALMAN_DEM]={0},Nav_G[KALMAN_DEM][KALMAN_NOISE]={0};    //状态转移矩阵
FP64 Nav_H[KALMAN_OBSERVE][KALMAN_DEM]={0};     //观测矩阵
FP64 Nav_P[KALMAN_DEM][KALMAN_DEM]={0},Nav_P0[KALMAN_DEM][KALMAN_DEM]={0},Nav_K[KALMAN_DEM][KALMAN_OBSERVE]={0};
FP64 Nav_Q[KALMAN_NOISE][KALMAN_NOISE]={0},Nav_R[KALMAN_OBSERVE][KALMAN_OBSERVE]={0};
//滤波器输出估计值
FP64 E_Attitude[3]={0};
FP64 E_Velocity[3]={0};
FP64 E_Position[3]={0};

double B[KALMAN_DEM][KALMAN_NOISE]={0};
double H[KALMAN_OBSERVE][KALMAN_DEM]={0};
// double Temp_1818_1[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_2[KALMAN_DEM][KALMAN_DEM]={0},Temp_1818_3[KALMAN_DEM][KALMAN_DEM]={0};
// double Temp_189_1[KALMAN_DEM][KALMAN_NOISE]={0};
// double Temp_183_1[KALMAN_DEM][KALMAN_OBSERVE]={0},Temp_183_2[KALMAN_DEM][KALMAN_OBSERVE]={0};
// double Temp_918_1[KALMAN_NOISE][KALMAN_DEM]={0};
// double Temp_33_1[KALMAN_OBSERVE][KALMAN_OBSERVE]={0},Temp_33_2[KALMAN_OBSERVE][KALMAN_OBSERVE]={0};
// double Temp_181_1[KALMAN_DEM]={0},Temp_181_2[KALMAN_DEM]={0};
// double Temp_31_1[KALMAN_OBSERVE]={0},Temp_31_2[KALMAN_OBSERVE]={0};

FP64 Ta[3]={0};      //加速度计误差的相关时间
FP64 Tg[3]={0};      //陀螺仪误差的相关时间

float a_R[3]={0},v_R[3]={0},p_R[3]={0};
float a_ins[3]={0},v_ins[3]={0},p_ins[3]={0};
float Fn[3]={0},others=0;

float a_R_1[3]={0},v_R_1[3]={0},p_R_1[3]={0};
float a_ins_1[3]={0},v_ins_1[3]={0},p_ins_1[3]={0},p_gps[3]={0},p_gps_1[3]={0};
float Fn_1[3]={0},others_1=0;
float q_1[4]={0};

int Cal_Count=0;   //记录程序运行次数
/*************************************************
  Function:        Matrix_Add
  Description:     实现维数相同的矩阵相加
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵相乘
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Init_Kalman(void)
{
	unsigned char i=0,j=0;
	for (i=0;i<KALMAN_OBSERVE;i++)
	{
		Tg[i]=300;
		Ta[i]=1000;
	}
	for (i=0;i<KALMAN_NOISE;i++)
	{
		for(j=0;j<KALMAN_NOISE;j++)     //初始系统噪声
		{
			if (i==j)
			{
				switch (i)
				{
				case 0: 
				case 1:
				case 2: Nav_Q[i][j]=(0.04/(57*3600))*(0.04/(57*3600));break;
				case 3: 
				case 4:
				case 5: Nav_Q[i][j]=(0.01/(57*3600))*(0.01/(57*3600));break;
				case 6: 
				case 7:
				case 8: Nav_Q[i][j]=(1e-3)*(1e-3);break;
				}
			}
		}
	}
	for (i=0;i<KALMAN_OBSERVE;i++)    //初始量测噪声
	{
		for (j=0;j<KALMAN_OBSERVE;j++)
		{
			if (i==j)
			{
				switch (i)
				{
				case 0:Nav_R[i][j]=2.337634513504725e-11; break;
				case 1:Nav_R[i][j]=2.225534678359716e-11; break;
				case 2:Nav_R[i][j]=8.813141168662313e+02; break;
				}
				Unit_Array_3[i][j]=1;
			}
		}
	}

	for (i=0;i<KALMAN_DEM;i++)
	{
		for (j=0;j<KALMAN_DEM;j++)
		{
			if (i==j)
			{
				switch (i)
				{
				case 0: 
				case 1:Nav_P0[i][j]=((double)((double)1/(double)(36*57)))*((double)((double)1/(double)(36*57)));break;
				case 2:Nav_P0[i][j]=((double)1/(double)57)*((double)1/(double)57);break;
				case 3: 
				case 4:
				case 5:Nav_P0[i][j]=0.0001*0.0001; break;
				case 6:Nav_P0[i][j]=0; break; 
				case 7:Nav_P0[i][j]=0; break;
				case 8:Nav_P0[i][j]=1; break;
				case 9: 
				case 10:
				case 11:Nav_P0[i][j]=(0.1/(double)(57*3600))*(0.1/(double)(57*3600)); break;
				case 12: 
				case 13:
				case 14:Nav_P0[i][j]=(0.04/(double)(57*3600))*(0.04/(double)(57*3600)); break;
				case 15: 
				case 16:
				case 17:Nav_P0[i][j]=(1e-4)*(1e-4); break;
				}
				Unit_Array_18[i][j]=1;
			}
		}
	}
}
/*************************************************
  Function:        Matrix_Add
  Description:     实现维数相同的矩阵相加
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵相乘
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void main(void)
{
	int i=0;
	double a[4][3]={{0,2,1},{1,3,2},{2,4,8},{1,7,5}};
	double b[3][4]={{0,2,1,3},{1,3,2,4},{2,0,1,6}};
	double d[4][4]={{0,2,1,3},{1,3,2,4},{2,0,1,6},{2,1,2,6}};
	double c[4][4]={0};
	unsigned char r=2,l=2;
	FILE *fpdr,*fdr;                   //打开数据文件的指针
	Init_Kalman();
	if ((fdr=fopen("Output.dat","w"))!=NULL)
	{
		printf("打开数据输出文件！\n");
	}
	if((fpdr=fopen("Data.dat","rb"))!=NULL)
	{
		printf("打开数据文件！\n开始进行组合导航计算\n");
 		while(!feof(fdr))                  
		{
			fscanf(fpdr,"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
				   &a_R[0],&a_R[1],&a_R[2],  //姿态真实值
				   &v_R[0],&v_R[1],&v_R[2],  //速度真实值
				   &p_R[0],&p_R[1],&p_R[2],  //位置真实值
				   &a_ins[0],&a_ins[1],&a_ins[2],
				   &v_ins[0],&v_ins[1],&v_ins[2],
				   &p_ins[0],&p_ins[1],&p_ins[2],
				   &q[0],&q[1],&q[2],&q[3],
				   &Fn[0],&Fn[1],&Fn[2],
				   &p_gps[0],&p_gps[1],&p_gps[2],
				   &others
				  );
			Cal_Count++;
			printf("程序运行到第 %d 步\n",Cal_Count);
			if (Cal_Count>1)
			{
				Ve=v_ins_1[0];
				Vn=v_ins_1[1];
				Vd=v_ins_1[2];
				Temp_La=p_ins_1[0];
				Temp_Al=p_ins_1[2];
				fe=Fn_1[0];
				fn=Fn_1[1];
				fd=Fn_1[2];

				Cnb[0][0]=1-2*(q_1[2]*q_1[2]+q_1[3]*q_1[3]);   
				Cnb[0][1]=2*(q_1[1]*q_1[2]-q_1[0]*q_1[3]);
				Cnb[0][2]=2*(q_1[1]*q_1[3]+q_1[0]*q_1[2]);
				Cnb[1][0]=2*(q_1[1]*q_1[2]+q_1[0]*q_1[3]);
				Cnb[1][1]=1-2*(q_1[1]*q_1[1]+q_1[3]*q_1[3]);
				Cnb[1][2]=2*(q_1[2]*q_1[3]-q_1[0]*q_1[1]);
				Cnb[2][0]=2*(q_1[1]*q_1[3]-q_1[0]*q_1[2]);                    
				Cnb[2][1]=2*(q_1[2]*q_1[3]+q_1[0]*q_1[1]); 
				Cnb[2][2]=1-2*(q_1[1]*q_1[1]+q_1[2]*q_1[2]);

				Nav_Z[0]=p_ins[0]-p_gps[0];//(double)(p_R[0]+30*rand()/32767/R);//
				Nav_Z[1]=p_ins[1]-p_gps[1];//(double)(p_R[1]+30*rand()/32767/R);//
				Nav_Z[2]=p_ins[2]-p_gps[2];//(double)(p_R[2]+30*rand()/32767);//
				Reload_Nav_Pra();
				Nav_Kalman();
			}

			a_R_1[0]=a_R[0];
			a_R_1[1]=a_R[1];
			a_R_1[2]=a_R[2];

			v_R_1[0]=v_R[0];
			v_R_1[1]=v_R[1];
			v_R_1[2]=v_R[2];

			p_R_1[0]=p_R[0];
			p_R_1[1]=p_R[1];
			p_R_1[2]=p_R[2];

			a_ins_1[0]=a_ins[0];
			a_ins_1[1]=a_ins[1];
			a_ins_1[2]=a_ins[2];

			v_ins_1[0]=v_ins[0];
			v_ins_1[1]=v_ins[1];
			v_ins_1[2]=v_ins[2];

			p_ins_1[0]=p_ins[0];
			p_ins_1[1]=p_ins[1];
			p_ins_1[2]=p_ins[2];

			p_gps_1[0]=p_gps[0];
			p_gps_1[1]=p_gps[1];
			p_gps_1[2]=p_gps[2];

			q_1[0]=q[0];
			q_1[1]=q[1];
			q_1[2]=q[2];
			q_1[3]=q[3];

			Fn_1[0]=Fn[0];
			Fn_1[1]=Fn[1];
			Fn_1[2]=Fn[2];
// 
			if (Nav_X[8]>1e10)
			{
				break;
			}
			fprintf(fdr,"%lf,%lf,%lf,%lf,%lf,%lf\n",
				    Nav_Z[0],Nav_Z[1],Nav_Z[2],Nav_X[6],Nav_X[7],Nav_X[8]);
// 			fscanf(fpdr,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
// 				&time,
// 				&Gx,&Gy,&Gz,&Ax,&Ay,&Az,
// 				&Phi,&Theta,&Gamma,
// 				&Vn,&Ve,&Vd,
// 				&Output_La,&Output_Lo,&Output_Al,
// 				&Temperature,&Satellite_Num,&Gps_La,&Gps_Lo,&Gps_UTC
// 				);   //读取SD3012
// 
// 			fprintf(fdr,"%5.8f,%5.8f,%5.8f,%5.8f,%5.8f\n",
// 				time,Gx,Gy,Gz,Temperature);
		}
	}
	else
	{
		printf("打开文件失败！\n");
	}
	printf("程序运行结束！\n");
	fclose(fdr);
	fclose(fpdr);
//	Matrix_Mul((double *)a,(double *)b,3,2,4,(double *)c);
//	Matrix_Inver((double *)a,4,3,(double *)c);
//	Matrix_Add((double *)b,(double *)b,3,4,(double *)c);
 	Matrix_Opp((double *)d,4,(double *)c);
 	printf("%f,%f,%f,%f\n%f,%f,%f,%f\n%f,%f,%f,%f\n%f,%f,%f,%f\n",c[0][0],c[0][1],c[0][2],c[0][3]
	                                                ,c[1][0],c[1][1],c[1][2],c[1][3]
													,c[2][0],c[2][1],c[2][2],c[2][3]
													,c[3][0],c[3][1],c[3][2],c[3][3]);
	getchar();
	getchar();
}