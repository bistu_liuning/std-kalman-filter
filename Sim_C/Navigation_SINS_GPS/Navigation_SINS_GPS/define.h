
#ifndef	__DEFINE_H__
#define	__DEFINE_H__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "Matrix_Fun.h"
#include "Algorithm.h"

/* 类型定义 */
typedef unsigned char   BOOLEAN;                 /* 布尔变量                                 */
typedef unsigned char  	INT8U;                   /* 无符号8位整型变量                        */
typedef signed   char  	INT8S;                   /* 有符号8位整型变量                        */
typedef unsigned int 	INT16U;                  /* 无符号16位整型变量                       */
typedef signed   int 	INT16S;                  /* 有符号16位整型变量                       */
typedef unsigned long   INT32U;                  /* 无符号32位整型变量                       */
typedef signed   long   INT32S;                  /* 有符号32位整型变量                       */
typedef float          	FP32;                    /* 单精度浮点数（32位长度）                 */
typedef double         	FP64;                    /* 双精度浮点数（64位长度）                 */


/* 常用数值定义*/
#define PI     3.1415926
#define espp   0.00000000001     //在计算矩阵求逆时，常常出现个别元素为零，为避免数据溢出，引入极小变量espp，用其代替0
#define g0     9.8
#define R      6378245                //地球半径
#define f      1/298.3                //地球扁率
#define e      1/298.257         //地球椭圆度
#define wie    7.292e-5              //地球自转角速度
#define KALMAN_DEM 18
#define KALMAN_OBSERVE 3             //卡尔曼滤波观测向量个数
#define KALMAN_NOISE   9             //卡尔曼滤波噪声维数

extern float time;         //系统时标
extern float Gx;           //x轴陀螺仪输出
extern float Gy;           //y轴陀螺仪输出
extern float Gz;           //y轴陀螺仪输出

extern float Ax;           //x轴加速度计输出
extern float Ay;           //y轴加速度计输出
extern float Az;           //y轴加速度计输出

extern float MAG_X;
extern float MAG_Y;
extern float MAG_Z;

extern float fn;
extern float fe;
extern float fd;

extern float Phi;          //偏航角输出
extern float Theta;        //俯仰角输出
extern float Gamma;        //横滚角输出

extern float Vn;           //北向速度
extern float Ve;           //东向速度
extern float Vd;           //地向速度

extern float Output_La;   //组合后纬度输出
extern float Output_Lo;   //组合后经度输出
extern float Output_Al;   //组合后高度输出

extern float Temperature;  //系统温度输出
extern float Satellite_Num;//卫星数
extern float Gps_La;       //GPS纬度输出
extern float Gps_Lo;       //GPS经度输出
extern float Gps_UTC;      //GPS UTC时间

/* 导航系统参数 */
extern FP64 A;	            //四元数归一参考值
extern FP64 q1[4];
extern FP32 q[4]; //存储四元数
extern FP64 T[3][3];      //转移矩阵
extern FP64 Cbn[3][3],Cnb[3][3];    //方向余弦矩阵
extern FP64 Temp_phi,Temp_theta,Temp_gamma;
extern FP64 Temp_phi,Temp_theta,Temp_gamma;		//三个姿态角
extern FP64 Temp_Vn,Temp_Ve,Temp_Vd;             //三个轴向速度
extern FP64 Temp_Sn,Temp_Se,Temp_Sd;             //三个轴向位置
extern FP64 Temp_La,Temp_Lo,Temp_Al;             //三个轴向位置
extern FP64 DATA;

/* KALMAN滤波器相关参数 */
extern FP64 Unit_Array_18[KALMAN_DEM][KALMAN_DEM],Unit_Array_3[KALMAN_OBSERVE][KALMAN_OBSERVE];
extern FP64 NAV_Tm;    //kalman滤波器更新时间
extern FP64 Nav_X[KALMAN_DEM],Nav_Z[KALMAN_OBSERVE];    //输入状态向量 7维
extern FP64 Nav_A[KALMAN_DEM][KALMAN_DEM],Nav_F[KALMAN_DEM][KALMAN_DEM],Nav_G[KALMAN_DEM][KALMAN_NOISE];    //状态转移矩阵
extern FP64 Nav_H[KALMAN_OBSERVE][KALMAN_DEM];     //观测矩阵
extern FP64 Nav_P[KALMAN_DEM][KALMAN_DEM],Nav_P0[KALMAN_DEM][KALMAN_DEM],Nav_K[KALMAN_DEM][KALMAN_OBSERVE];
extern FP64 Nav_Q[KALMAN_NOISE][KALMAN_NOISE],Nav_R[KALMAN_OBSERVE][KALMAN_OBSERVE];
extern FP64 Ta[];      //加速度计误差的相关时间
extern FP64 Tg[];      //陀螺仪误差的相关时间
//滤波器输出估计值
extern FP64 E_Attitude[];
extern FP64 E_Velocity[];
extern FP64 E_Position[];

extern double B[KALMAN_DEM][KALMAN_NOISE];
extern double H[KALMAN_OBSERVE][KALMAN_DEM];
// extern double Temp_1818_1[KALMAN_DEM][KALMAN_DEM],Temp_1818_2[KALMAN_DEM][KALMAN_DEM],Temp_1818_3[KALMAN_DEM][KALMAN_DEM],Temp_1818_4[KALMAN_DEM][KALMAN_DEM],Temp_1818_5[KALMAN_DEM][KALMAN_DEM];
// extern double Temp_1818_6[KALMAN_DEM][KALMAN_DEM],Temp_1818_7[KALMAN_DEM][KALMAN_DEM],Temp_1818_8[KALMAN_DEM][KALMAN_DEM],Temp_1818_9[KALMAN_DEM][KALMAN_DEM],Temp_1818_10[KALMAN_DEM][KALMAN_DEM];
// extern double Temp_1818_11[KALMAN_DEM][KALMAN_DEM],Temp_1818_12[KALMAN_DEM][KALMAN_DEM];
// extern double Temp_189_1[KALMAN_DEM][KALMAN_NOISE],Temp_189_2[KALMAN_DEM][KALMAN_NOISE];
// extern double Temp_183_1[KALMAN_DEM][KALMAN_OBSERVE],Temp_183_2[KALMAN_DEM][KALMAN_OBSERVE];
// extern double Temp_918_1[KALMAN_NOISE][KALMAN_DEM];
// extern double Temp_33_1[KALMAN_OBSERVE][KALMAN_OBSERVE],Temp_33_2[KALMAN_OBSERVE][KALMAN_OBSERVE],Temp_33_3[KALMAN_OBSERVE][KALMAN_OBSERVE];
// extern double Temp_181_1[KALMAN_DEM],Temp_181_2[KALMAN_DEM];
// extern double Temp_31_1[KALMAN_OBSERVE],Temp_31_2[KALMAN_OBSERVE];

extern FP64 Tm;           //姿态解算计算时间
#endif