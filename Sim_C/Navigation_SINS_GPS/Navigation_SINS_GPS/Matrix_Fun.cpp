/************************************************************
  Copyright (C), 2009-2016, 北京信息科技大学智能控制研究所. Co., Ltd.
  FileName: Matrix_Fun.cpp
  Author:   刘宁    
  Version : 1.0          
  Date:     2012.4.13
  Description:     实现矩阵操作      
  Function List:   // 主要函数及其功能
    1. void Matrix_Add(...) 矩阵相加
	2.
	3.
  History:         // 历史修改记录
      <author>  <time>   <version >   <desc>
      David    96/10/12     1.0     build this moudle  
***********************************************************/
#include "define.h"
/*************************************************
  Function:        Matrix_Add
  Description:     实现维数相同的矩阵相加
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵相乘
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Add(double *Input_A , double *Input_B ,unsigned char In_Row,unsigned char In_Line,double *Output)
{
	int i=0;
	int j=0;

	for (i=0;i<In_Row;i++)
	{
		for (j=0;j<In_Line;j++)
		{
			Output[i*In_Line+j]=Input_A[i*In_Line+j]+Input_B[i*In_Line+j];
		}
	}
}
/*************************************************
  Function:        Matrix_Sub
  Description:     实现维数相同的矩阵相减
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Sub(double *Input_A , double *Input_B ,unsigned char In_Row,unsigned char In_Line,double *Output)
{
	int i=0;
	int j=0;

	for (i=0;i<In_Row;i++)
	{
		for (j=0;j<In_Line;j++)
		{
			Output[i*In_Line+j]=Input_A[i*In_Line+j]-Input_B[i*In_Line+j];
		}
	}
}
/*************************************************
  Function:        Matrix_Mul
  Description:     实现维数相同的矩阵相乘
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char Mid    :
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Mul(double *Input_A , double *Input_B ,unsigned char Output_Row,unsigned char Mid,unsigned char Output_Line,double *Output)
{
	int Row=0,Line=0,m=0;
	for (Row=0;Row<Output_Row;Row++)
	{
		for (Line=0;Line<Output_Line;Line++)
		{
			for (m=0;m<Mid;m++)
			{
				Output[Row*Output_Line+Line]=Output[Row*Output_Line+Line]+Input_A[Row*Mid+m]*Input_B[m*Output_Line+Line];
			}
		}
	}
}
/*************************************************
  Function:        Matrix_Inver
  Description:     实现维数相同的矩阵转置
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   double *Input_B      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char Mid    :
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Inver(double *Input_A,unsigned char In_Row,unsigned char In_Line,double *Output)
{
	int Row=0,Line=0;
	for (Row=0;Row<In_Line;Row++)
	{
		for (Line=0;Line<In_Row;Line++)
		{
             Output[Row*In_Row+Line]=Input_A[Line*In_Line+Row];
		}
	}
}
/*************************************************
  Function:        Matrix_Opp
  Description:     实现维数相同的矩阵求逆
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Opp(double *Input_A,unsigned char In_Row,double *Output)
{
	int Row=0,Line=0;
	double t=0,x=0,j=0;
	double *Temp=0;
	
	Temp=(double *)malloc(In_Row*2*In_Row*sizeof(double));   //开辟动态内存空间，用于计算临时矩阵数据
	
	for (Row=0;Row<In_Row;Row++)
	{
		for (Line=0;Line<2*In_Row;Line++)                     //拓宽数组,拓宽单位矩阵
		{
			if (Line<In_Row)
			{
				Temp[Row*2*In_Row+Line]=Input_A[Row*In_Row+Line];
			}
			else if (Line==(In_Row+Row))
			{
				Temp[Row*2*In_Row+Line]=1;
			}
			else
			{
				Temp[Row*2*In_Row+Line]=0;
			}
		}
	}

	for (Row=0;Row<In_Row;Row++)
	{
		for (Line=0;Line<In_Row;Line++)
		{
			if (Line!=Row)
			{
				if (Temp[Row*2*In_Row+Row]!=0)
				{
					t=Temp[Line*2*In_Row+Row]/Temp[Row*2*In_Row+Row];
				}
				else
					t=Temp[Line*2*In_Row+Row]/espp;
				for (int j=0;j<2*In_Row;j++)
				{
					x=Temp[Row*2*In_Row+j]*t;
					Temp[Line*2*In_Row+j]=Temp[Line*2*In_Row+j]-x;
				}
			}
		}
	}

	for (Row=0;Row<In_Row;Row++)
	{
		t=Temp[Row*2*In_Row+Row];
		for (Line=0;Line<2*In_Row;Line++)
		{
			if(t!=0)
			{
				Temp[Row*2*In_Row+Line]=Temp[Row*2*In_Row+Line]/t;
			}
			else
				Temp[Row*2*In_Row+Line]=Temp[Row*2*In_Row+Line]/espp;
		}
	}

	for (Row=0;Row<In_Row;Row++)
	{
		for (Line=0;Line<In_Row;Line++)
		{
			Output[Row*In_Row+Line]=Temp[Row*2*In_Row+Line+In_Row];
		}
	}
	free(Temp);
}
/*************************************************
  Function:        Matrix_Mul_Digit
  Description:     实现矩阵乘以一个数
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double Digit         ：待乘数字值
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Mul_Digit(double *Input_A,unsigned char In_Row,unsigned char In_Line,double Digit,double *Output)
{
	int Row=0,Line=0;   //定义矩阵运算过程中的行、列值
	for (Row=0;Row<In_Row;Row++)
	{
		for (Line=0;Line<In_Line;Line++)
		{
			Output[Row*In_Line+Line]=Input_A[Row*In_Line+Line]*Digit;
		}
	}
}
/*************************************************
  Function:        Matrix_Diag
  Description:     计算矩阵对角线上元素
  Calls:           无
  Called By:       主要用于Kalman滤波中的矩阵计算
  Input:           double *Input_A      ：输入矩阵对应地址
                   unsigned char In_Row ：矩阵行
				   unsigned char In_Line：矩阵列
				   double *Output       ：相加后输出的矩阵
  Output:          对输出参数的说明。
  Return:          函数返回值的说明
  Others:          
*************************************************/
void Matrix_Diag(double *Input_A,unsigned char In_Row,unsigned char In_Line,double *Output)
{
	int Row=0,Line=0;   //定义矩阵运算过程中的行、列值
	for (Row=0;Row<In_Row;Row++)
	{
		for (Line=0;Line<In_Line;Line++)
		{
			if (Row==Line)
			{
				Output[Row]=Input_A[Row*In_Line+Line];
			}
		}
	}
}
