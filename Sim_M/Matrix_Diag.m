function [ output ] = Matrix_Diag( A )
%MATRIX_DIAG Summary of this function goes here
%   求取矩阵对角线元素
[D_R,D_L]=size(A);
output=zeros(D_R,1);
for Row=1:1:D_R
    for Line=1:1:D_L
        if(Row==Line)
            output(Row)=A(Row,Line);
        end
    end
end

end

