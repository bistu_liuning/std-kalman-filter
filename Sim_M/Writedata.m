clear all;
clc;
close all;
load ode500.mat
fid=fopen('Data.dat','wt');
[Row,Line]=size(yout);
for i=1:10:Row
    for j=1:1:Line
        if(j<Line)
            fprintf(fid,'%s,',mat2str(yout(i,j)));
        else
            fprintf(fid,'%s\n',mat2str(yout(i,j)));
        end
    end
end
fclose(fid);