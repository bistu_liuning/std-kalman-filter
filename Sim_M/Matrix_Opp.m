function [ output ] = Matrix_Opp( A )
%MATRIX_OPP Summary of this function goes here
%   ��������
[D_R,D_L]=size(A);
Temp1=zeros(D_R,D_L);
Temp2=zeros(D_R,2*D_L);
y=1;
for Row=1:1:D_R
    for Line=1:1:2*D_R
        if(Line<=D_R)
            Temp2(Row,Line)=A(Row,Line);
        elseif(Line==(D_R+Row))
            Temp2(Row,Line)=1;
        else
            Temp2(Row,Line)=0;
        end
    end
end

for Row=1:1:D_R
    for Line=1:1:D_R
        if(Line~=Row)
			if(Temp2(Row,Row)~=0)
                t=Temp2(Line,Row)/Temp2(Row,Row);
			else 
			   t=Temp2(Line,Row)/0.00001;
            end
			for j=1:1:2*D_R 
				x=Temp2(Row,j)*t; 
				Temp2(Line,j)=Temp2(Line,j)-x; 
            end
        end
    end
end

for Row=1:1:D_R
    t=Temp2(Row,Row); 
    for j=1:1:2*D_R
        if(t~=0) 
            Temp2(Row,j)=Temp2(Row,j)/t;
        else 
              Temp2(Row,j)=Temp2(Row,j)/0.00001;  
        end
    end
end

for j=1:1:D_R
   y=y*Temp2(j,j);  
end

for Row=1:1:D_R
  for Line=1:1:D_R
        output(Row,Line)=Temp2(Row,Line+D_R); 
  end
end

end

