function [ output ] = Matrix_Mul( A,B )
%MATRIX_MUL Summary of this function goes here
%   �������
[D_R,D_L]=size(A);
[D_R_2,D_L_2]=size(B);
output=zeros(D_R,D_L_2);
for row=1:1:D_R
    for line=1:1:D_L_2
        for m=1:1:D_L
            output(row,line)=output(row,line)+A(row,m)*B(m,line);
        end
    end
end

end

